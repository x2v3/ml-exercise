#!/usr/bin/python3

# 书中的例子是python2的，这里用的是python3.

from numpy import *
from os import listdir
import operator
import matplotlib
import matplotlib.pyplot as plt


def createDataset():
    group = array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]])
    labels = ['A', 'A', 'B', 'B']
    return group, labels


def classify0(inX, labels, dataSet, k):
    dataSetSize = dataSet.shape[0]
    # print('dataSetSize is:\n %s' % dataSetSize)

    diffMat = tile(inX, (dataSetSize, 1)) - dataSet
    # print('diffMat is:\n %s' % diffMat)

    sqDiffMat = diffMat ** 2
    # print('sqDiffMat is:\n %s' % sqDiffMat)

    sqDistances = sqDiffMat.sum(axis=1)
    # print('sqDistances is:\n %s' % sqDistances)

    distances = sqDistances ** 0.5
    # print('distances is:\n %s' % distances)

    sortedDistIndicies = distances.argsort()
    classCount = {}

    for i in range(k):
        voteILable = labels[sortedDistIndicies[i]]
        classCount[voteILable] = classCount.get(voteILable, 0) + 1
        # print(classCount)
        sortedClassCount = sorted(classCount.items(), key=lambda x: x[1], reverse=True)

    return sortedClassCount[0][0]


def file2matrix(filename):
    f = open(filename)
    lines = f.readlines()
    f.close()
    number_of_lines = len(lines)
    # zero(shape) 用指定的shape创建用0填充的matrix
    return_mat = zeros((number_of_lines, 3))
    class_label_vector = []
    index = 0
    for l in lines:
        line_array = l.strip().split('\t')
        return_mat[index, :] = line_array[0:3]
        # 从网站下载下来的代码中，最后一列是文字描述 ，并不像书中给出的是数字。所以这里要转成数字。
        str2int = lambda x: 1 if x == 'didntLike' else 2 if x == 'smallDoses' else 3
        class_label_vector.append(str2int(line_array[-1]))
        index += 1
    return return_mat, class_label_vector


def auto_norm(dataset):
    min_vals = dataset.min(0)
    max_vals = dataset.max(0)
    # print('min value is %s, max value is %s' % (min_vals, max_vals))
    ranges = max_vals - min_vals
    # print(ranges)
    # tile(一个值,(在各维度各自重复多少次))
    tiled_min_values = tile(min_vals, (dataset.shape[0], 1))
    tiled_ranges = tile(ranges, (dataset.shape[0], 1))
    # 归一化用的公式： (oldValue-min)/range  # range=max-min
    ret_dataset = (dataset - tiled_min_values) / tiled_ranges
    # print(ret_dataset)
    return ret_dataset


def classify1(sample, sample_lables, data_to_classify, validation):  # 2.2 dating classify
    # 自己根据理解写的，可能和书里不一样，data to classify 是一个点。
    distances = zeros((sample.shape[0], 2))
    tiled_data_to_classify = tile(data_to_classify, (sample.shape[0], 1))
    distances[0:, 0] += sample_lables
    # 在这里调试了好外，之前是公式用错了。
    # 正确的公式应该是 sqrt((x1-x2)**2 + (y1-y2)**2)
    d0 = (tiled_data_to_classify - sample) ** 2
    d1 = d0.sum(axis=1)
    d = d1 ** 0.5
    distances[:, 1] += d
    # 找到距离最近的N个，这里N取20
    sd = sorted(distances, key=lambda x: x[1])
    # print(sd)
    top20 = sd[0:20]
    # print(top20)
    classcount = {}
    for i in top20:
        cls = str(i[0])
        if not cls in classcount.keys():
            classcount[cls] = 0
        else:
            classcount[cls] = classcount[cls] + 1

    classified = sorted(classcount, key=lambda x: classcount[x], reverse=True)[0]
    print('classified as:%s, the validation is %s' % (classified, validation))
    return int(float(classified)) == int(validation)


# 2.3 的读取样本
def num_matrix(filepath):
    labels = []
    files = [f for f in listdir(filepath) if f[-3:] == 'txt']  # all the files ends with txt
    files_content = zeros((len(files), 32 * 32))  # there are 32 lines * 32 columns in each file.
    index = 0
    for f in files:
        # print('reading file:%s'%f)
        reader = open(filepath + '/' + f)
        chars = []
        lines = reader.readlines()
        reader.close()
        for l in lines:
            for c in l.strip():
                chars.append(c)
        files_content[index,] = chars
        labels.append(f[0:1])
        index += 1
    # print(files_content)
    # print(len(labels))
    return files_content, labels


# 2.3 的数字识别
def classify_num(sample, labels, data_to_classify):
    # 因为都是0和1，所以没有归一化的必要
    # 首先把data_to_classify做成和sample一样的形状
    tiled_data = tile(data_to_classify, (sample.shape[0], 1))
    # 距离公式 sqrt((x1-x2)**2 + (y1-y2)**2)
    sq_diff = (tiled_data - sample) ** 2
    sum_sq_diff = sq_diff.sum(axis=1)
    # 创建一个矩阵，用来保存label和距离的映射关系
    distance = zeros((len(sample), 2))
    distance[0:, 0] = labels
    distance[0:, 1] = sum_sq_diff ** 0.5

    # 排序，找到距离最近的N个点,这里我原来使用了
    sd = sorted(distance, key=lambda x: x[1])
    top_n = sd[0:50]

    # 从前N中找出最多的标签
    label_count = {}
    for lc in top_n:
        l = lc[0]
        if l in label_count.keys():
            label_count[l] += 1
        else:
            label_count[l] = 1
    # print(label_count)
    sorted_labels = sorted(label_count, key=lambda x: label_count[x], reverse=True)
    return sorted_labels[0]


def main():
    print('start')
    # 2.1 练习
    # g, l = createDataset()
    # inX = [0, 0]
    # classify0(inX, l, g, 2)

    # 2.2练习
    # dating_mat, dating_label = file2matrix('data/datingTestSet.txt')
    # # print(dating_label)
    # # fig = plt.figure()
    # # ax = fig.add_subplot(111)
    # # ax.scatter(dating_mat[:, 1], dating_mat[:, 2],15.0*array(dating_label),15.0*array(dating_label))
    # # plt.show()
    # dataset_to_classify = auto_norm(dating_mat)
    # sample = dating_mat[:800, ]
    # # print(sample)
    # sample_labels = dating_label[:800]
    # # print(sample_labels)
    # dataset_to_classify = dating_mat[801:, ]
    # # print(dataset_to_classify)
    # validation_labels = dating_label[801:]
    # # print(validation_labels)
    # classified_count=len(dataset_to_classify)
    # error_count=0
    # for i in range(classified_count):
    #     r = classify1(sample, sample_labels, dataset_to_classify[i], validation_labels[i])
    #     if not r:
    #         error_count+=1
    # print('error rate:%s'% str(float(error_count)/float(classified_count)))

    sample, labels = num_matrix('data/digits/trainingDigits')
    data_to_classify, validation = num_matrix('data/digits/testDigits')
    error_count = 0
    total_count = len(data_to_classify)
    for i in range(total_count):
        # r1= classify0(data_to_classify[i],labels,sample,20)
        r = classify_num(sample, labels, data_to_classify[i, 0:])
        print('the %s number is %s,is classified as %s (num) ' % (i, validation[i], r))
        if int(float(r)) != int(float(validation[i])):
            error_count += 1
            # break
    print('error rate is %s' % (float(error_count) / float(total_count)))


if __name__ == '__main__':
    main()
