from numpy import *
from math import log as mathlog


# 计算公式不太好打，现查吧……
def calc_shannon_entropy(dataset):
    # dataset是一个二维数组，数组的最后一列是类别标签，前面的列都是属性。
    data_count = len(dataset)
    # 计算每个类别的数量
    labels_count = {}
    for d in dataset:
        # 每一行的最后一列是标签，前面都是属性值
        label = d[-1]
        if label in labels_count.keys():
            labels_count[label] += 1
        else:
            labels_count[label] = 1
    # 计算熵值
    entropy = 0.0
    for l in labels_count:
        prob = float(labels_count[l]) / data_count  # 这个值出现的概率
        lx = prob * mathlog(prob, 2)
        entropy -= lx
    return entropy

def create_dataset():
    dataset = [[1, 1, 'yes'], [1, 1, 'yes'], [1, 0, 'no'], [0, 1, 'no'], [0, 1, 'no']]
    labels = ['no surfacing',' flippers']
    return dataset, labels


def main():
    ds,labels=create_dataset()
    entropy=calc_shannon_entropy(ds)
    print(entropy)

if __name__ == '__main__':
    main()